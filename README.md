# nanocomp Singularity container
Bionformatics package nanocomp<br>
Comparing runs of Oxford Nanopore sequencing data and alignments
https://github.com/wdecoster/NanoComp
nanocomp Version: 1.10.1<br>

Singularity container based on the recipe: Singularity.nanocomp_v1.10.1

Package installation using Miniconda3 V4.7.12<br>

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

can be pull (singularity version >=3.3) with:<br>
singularity pull nanocomp_v1.10.1.sif oras://registry.forgemia.inra.fr/gafl/singularity/nanocomp/nanocomp:latest


